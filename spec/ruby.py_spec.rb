# rubocop:disable Metrics/BlockLength
# rubocop:disable Style/SpecialGlobalVars
RSpec.describe RubyPy do
  context 'standard modules' do
    context 'curses.ascii' do
      it 'is importable' do
        ca = RubyPy.import('curses.ascii')
        expect(ca).not_to be_nil
      end
    end

    context 're' do
      it 'is usable' do
        re = RubyPy.import('re')

        pattern = re.compile('d')
        expect(pattern.search('dog')).not_to be nil
        expect(pattern.search('dog', 1)).to be nil

        pattern = re.compile('o')
        expect(pattern.match('dog')).to be nil
        expect(pattern.match('dog', 1)).not_to be nil

        begin
          pattern = re.compile('o[gh]')
          expect(pattern.fullmatch('dog')).to be nil
          expect(pattern.fullmatch('ogre')).to be nil
          expect(pattern.fullmatch('doggie', 1, 3)).not_to be nil
        rescue NoMethodError, /fullmatch/
          'New in version 3.4.'
        end

        m = re.match('(\w+) (\w+)', 'Isaac Newton, physicist')
        expect(m.group(0)).to eq 'Isaac Newton'
        expect(m.group(1)).to eq 'Isaac'
        expect(m.group(2)).to eq 'Newton'
        expect(m.group(1, 2)).to eq PyCall::Tuple.new('Isaac', 'Newton')
      end
    end

    context 'string' do
      it 'is usable' do
        string = RubyPy.import('string')

        expect(string.ascii_lowercase).to eq(('a'..'z').to_a.join)

        fs = string.Formatter.new.format('The sum of 1 + 2 is {0}', 3)
        expect(fs).to eq 'The sum of 1 + 2 is 3'

        ts = string.Template.new('$subject $object $verb')
                   .substitute(subject: 'S', object: 'O', verb: 'V')
        expect(ts).to eq 'S O V'

        expect(string.capwords('this is a test')). to eq 'This Is A Test'
      end
    end
  end

  context 'optional modules' do
    context 'numpy' do
      it 'is usable' do
        begin
          np = RubyPy.import('numpy')

          a = np.arange(15).reshape(3, 5)
          expect(a.shape.inspect).to eq '(3, 5)'
          expect(a.ndim).to eq 2
          expect(a.dtype.name).to eq 'int64'
          expect(a.itemsize).to eq 8
          expect(a.size).to eq 15
        rescue PyCall::PyError, /No module named/
          skip $!.message
        end
      end
    end

    context 'tensorflow' do
      it 'is usable' do
        begin
          ENV['TF_CPP_MIN_LOG_LEVEL'] = '2'
          tf = RubyPy.import('tensorflow.compat.v1')
          tf.disable_v2_behavior

          sess   = tf.Session.new
          text   = 'Hello, TensorFlow!'
          hello  = tf.constant text
          result = sess.run(hello)
          expect(result).to eq text

          sess = tf.Session.new
          a    = tf.constant 2
          b    = tf.constant 3
          result = sess.run(a + b)
          expect(result).to eq 5
          result = sess.run(a * b)
          expect(result).to eq 6

          sess   = tf.Session.new
          a      = tf.placeholder tf.int16
          b      = tf.placeholder tf.int16
          add    = tf.add(a, b)
          mul    = tf.multiply(a, b)
          result = sess.run(add, feed_dict: { a => 2, b => 3 })
          expect(result).to eq 5
          result = sess.run(add, feed_dict: { a => 3, b => 3 })
          expect(result).to eq 6
          result = sess.run(mul, feed_dict: { a => 4, b => 3 })
          expect(result).to eq 12

          sess    = tf.Session.new
          matrix1 = tf.constant([[3.0, 3.0]])
          matrix2 = tf.constant([[2.0], [2.0]])
          product = tf.matmul(matrix1, matrix2)
          result  = sess.run(product)
          expect(result).to eq [[12.0]]
        rescue PyCall::PyError, /No module named/
          skip $!.message
        end
      end
    end
  end

  context 'code injection' do
    it 'should fail safely' do
      expect { RubyPy.import 'ce; raise("BOO")' }.to raise_error(ArgumentError)
    end
  end

  context 'lazy py-calling' do
    it 'should only load a python module once' do
      m1 = RubyPy.import('string')
      m2 = RubyPy.import('string')
      m3 = RubyPy.import('string')

      expect(m1._pycall).to equal m2._pycall
      expect(m2._pycall).to equal m3._pycall
      expect(m3._pycall).to equal m1._pycall
    end

    it 'should not load the python module until it is used' do
      pm = RubyPy.import('posixpath')
      expect(pm.instance_variable_get(:@_pycall)).to be nil
      pm.supports_unicode_filenames
      expect(pm.instance_variable_get(:@_pycall)).not_to be nil
    end
  end
end
# rubocop:enable Style/SpecialGlobalVars
# rubocop:enable Metrics/BlockLength
